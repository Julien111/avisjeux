package com.avisjeux.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avisjeux.core.business.Genre;

public interface GenreDao extends JpaRepository<Genre, Long> {

}
