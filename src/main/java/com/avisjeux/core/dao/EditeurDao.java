package com.avisjeux.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avisjeux.core.business.Editeur;

public interface EditeurDao extends JpaRepository<Editeur, Long> {

}
