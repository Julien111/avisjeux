package com.avisjeux.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avisjeux.core.business.Classification;

public interface ClassificationDao extends JpaRepository<Classification, Long> {

}
