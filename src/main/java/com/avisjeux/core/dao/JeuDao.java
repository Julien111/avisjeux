package com.avisjeux.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avisjeux.core.business.Jeu;

public interface JeuDao extends JpaRepository<Jeu, Long> {

}
