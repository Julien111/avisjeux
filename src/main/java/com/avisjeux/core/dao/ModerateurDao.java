package com.avisjeux.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avisjeux.core.business.Moderateur;

public interface ModerateurDao extends JpaRepository<Moderateur, Long> {

}
