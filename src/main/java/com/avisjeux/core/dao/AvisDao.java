package com.avisjeux.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avisjeux.core.business.Avis;

public interface AvisDao  extends JpaRepository<Avis, Long> {

}
