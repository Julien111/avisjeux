package com.avisjeux.core.business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name="utilisateur")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public abstract class Utilisateur {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	
	@Column(name="pseudo")
	@NotBlank
	protected String pseudo;
	
	@Column(name = "mot_de_passe")
	@NotBlank
	@Length(min=8, max=200, message="Le mot de passe doit être compris entre 8 et 200 caractères")
	protected String motDePasse;
	
	@Column(name = "email", unique=true)
	@NotBlank
	protected String email;

	protected Utilisateur() {		
		super();
	}	

}
