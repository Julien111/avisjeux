package com.avisjeux.core.business;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@Entity
public class Joueur extends Utilisateur {
	
	@Column(name="date_de_naissance")	
	@NotBlank
	private LocalDate dateDeNaissance;
	
	@OneToMany(mappedBy="joueur", fetch=FetchType.EAGER)
	private List<Avis> listeAvis;

	public Joueur() {
		super();
	}

}
