package com.avisjeux.core.business;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name="jeu")
public class Jeu {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
	private String nom;
	
	@Lob
	@NotBlank
	private String description;
		
	@Column(name="date_sortie")
	@NotNull
	private LocalDate dateSortie;
	
	@NotBlank
	private String image;
	
	@OneToMany(mappedBy="jeu", fetch=FetchType.EAGER)
	private List<Avis> listeAvis;
	
	@ManyToOne
	@JoinColumn(name="id_moderateur")
	private Moderateur moderateur ;	
	
	@ManyToOne
	@JoinColumn(name="id_modele_economique")
	private ModeleEconomique modeleEco ;	
	
	@ManyToMany
	@JoinTable(name = "jeu_plateforme")
	private List<Plateforme> listePlateformes ;
	
	@ManyToOne
	@JoinColumn(name="id_editeur")
	private Editeur editeur ;
	
	@ManyToOne
	@JoinColumn(name="id_genre")
	private Genre genre;
	
	@ManyToOne
	@JoinColumn(name="id_classification")
	private Classification classification;

	public Jeu() {
		
	}

}
