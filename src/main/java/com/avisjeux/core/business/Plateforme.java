package com.avisjeux.core.business;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name="plateforme")
public class Plateforme {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="nom")
	@NotBlank
	@Length(min=2, max=50, message="Le nom  doit contenir entre 2 et 50 caractères")
	private String nom;
	
	@ManyToMany(mappedBy = "listePlateformes")
	private List<Jeu> listeJeux;

	public Plateforme() {
		super();
	}

}
