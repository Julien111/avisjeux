package com.avisjeux.core.business;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name="avis")
public class Avis {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="description")
	@NotBlank(message="Merci de saisir une description")
	@Lob
	private String description;
	
	@Column(name="date_envoi")
	@NotNull
	@FutureOrPresent
	private LocalDateTime dateEnvoi;
	
	@Column(name="float")
	@NotNull
	@PositiveOrZero
	private Float note;
	
	@Column(name="date_moderation")
	@NotNull
	private LocalDateTime dateModeration;
	
	@ManyToOne
	private Joueur joueur;
	
	@ManyToOne
	@JoinColumn(name="id_moderateur")
	private Moderateur moderateur ;	
	
	@ManyToOne
	private Jeu jeu;

	public Avis() {
		super();
	}

}
