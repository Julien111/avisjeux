package com.avisjeux.core.business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@Entity
public class Moderateur extends Utilisateur {
	
	@Column(name="numero_de_telephone")
	@Length(max=10)
	@NotBlank
	private String numeroDeTelephone;	

	public Moderateur() {
		super();
	}

}
